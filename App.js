import React, { Component } from 'react';
import { StyleSheet, View, Text, Button, AsyncStorage, TextInput } from 'react-native';
import PushNotification from 'react-native-push-notification';
import firebase from 'react-native-firebase';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      body: ''
    };
  }

  async componentDidMount() {
    PushNotification.configure({
      onNotification: function (notification) {
        console.log("NOTIFICATION:", notification);
      },
    });
  }

  async NotiPermission() {
    firebase.messaging().requestPermission()
      .then(() => {
        console.log("User has authorized");
      })
      .catch(error => {
        console.log("User has reject per")
      })
  }

  async componentDidMount() {
    firebase.messaging().hasPermission()
      .then(enable => {
        if (enable) {
          console.log("user has permission");
        } else {
          console.log("user doesn't have permission");
          this.NotiPermission();
        }
      });

    let fcmToken = await AsyncStorage.getItem('fcmToken');
    console.log("fcmToken from AsyncStorage: ", fcmToken);
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        console.log("fcmToken from AsyncStorage: ", fcmToken);
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }
  }

  sendNotificationToLoacl() {
    PushNotification.localNotificationSchedule({
      title: "this.state.title",
      message: "this.state.body", // (required)
      date: new Date(Date.now() + 60 * 1000) // in 60 secs
    });
  }

  async sendNotificationToGCM() {
    const FiREBASE_API_KEY = "AIzaSyDNrwj6gPIm7673WvGPEvuL8MjCl6P_Ojs";
    const registration_ids = await AsyncStorage.getItem('fcmToken');
    const message = {
      registration_ids: [registration_ids],
      notification: {
        title: this.state.title,
        body: this.state.body,
        "vibrate": 1,
        "sound": 1,
        "show_in_foreground": true,
        "priority": "hight",
        "content_available": true,
      }
    }

    let headers = new Headers({
      "Content-Type": "application/json",
      "Authorization": "key=" + FiREBASE_API_KEY
    });

    let response = await fetch("https://fcm.googleapis.com/fcm/send", {
      method: "POST",
      headers,
      body: JSON.stringify(message)
    });

    response = await response.json();
    console.log(response);

  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.input}
          placeholder="Enter title"
          onChangeText={(title) => this.setState({ title: title })}
        />

        <TextInput
          style={styles.input}
          placeholder="Enter content"
          onChangeText={(body) => this.setState({ body: body })}
        />

        <Button
          title="Send Notification To GCM"
          onPress={() => {
            this.sendNotificationToGCM();
            console.log(`Title: ${this.state.title} Body: ${this.state.body}`)
          }}
        />

        {/* <Button
          title="Send Notification To Local"
          onPress={() => {
            this.sendNotificationToLoacl();
            console.log(`Title: ${this.state.title} Body: ${this.state.body}`)
          }}
        /> */}

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  input: {
    paddingLeft: 20,
    borderRadius: 50,
    height: 50,
    width: 250,
    fontSize: 25,
    backgroundColor: 'white',
    borderColor: '#1abc9c',
    borderWidth: 1,
    marginBottom: 20,
    color: '#34495e'
  }
})
